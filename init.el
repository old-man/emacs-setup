(require 'package)
(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
                    (not (gnutls-available-p))))
       (proto (if no-ssl "http" "https")))
  (when no-ssl
    (warn "\
Your version of Emacs does not support SSL connections,
which is unsafe because it allows man-in-the-middle attacks.
There are two things you can do about this warning:
1. Install an Emacs version that does support SSL and be safe.
2. Remove this warning from your init file so you won't see it again."))
  ;; Comment/uncomment these two lines to enable/disable MELPA and MELPA Stable as desired
  ;;(add-to-list 'package-archives (cons "melpa" (concat proto "://melpa.org/packages/")) t)
  (add-to-list 'package-archives (cons "melpa-stable" (concat proto "://stable.melpa.org/packages/")) t)
  (when (< emacs-major-version 24)
    ;; For important compatibility libraries like cl-lib
    (add-to-list 'package-archives (cons "gnu" (concat proto "://elpa.gnu.org/packages/")))))

;;=======================================================================================================

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

;;=======================================================================================================

(setq initial-scratch-message "")
(setq inhibit-startup-message t)
(setq visible-bell t)

;;=======================================================================================================

(setq make-backup-files nil) ; no backups

;(setq backup-directory-alist '(("." . "~/.emacs.d/backup"))
;  backup-by-copying t    ; Don't delink hardlinks
;  version-control t      ; Use version numbers on backups
;  delete-old-versions t  ; Automatically delete excess backups
;  kept-new-versions 20   ; how many of the newest versions to keep
;  kept-old-versions 5    ; and how many of the old
;  )

;;=======================================================================================================

(load "~/.emacs.d/fav/android.el")
;(init-android)

(global-set-key (kbd "C-c a") (lambda () (interactive) (init-android)))

;;=======================================================================================================

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes (quote (tsdh-dark)))
 '(package-selected-packages (quote (magit powershell))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;;=======================================================================================================

; default text mode

(setq initial-major-mode 'text-mode)
(setq-default major-mode 'text-mode)

;;=======================================================================================================

; allow emacs to save and load desktop session with multiple frames when emacs is in Terminal (not GUI)

(setq desktop-restore-forces-onscreen nil)
(add-hook 'desktop-after-read-hook
          (
           lambda ()
                  (
                   frameset-restore
                   desktop-saved-frameset
                   :reuse-frames (eq desktop-restore-reuses-frames t)
                   :cleanup-frames (not (eq desktop-restore-reuses-frames 'keep))
                   :force-display desktop-restore-in-current-display
                   :force-onscreen desktop-restore-forces-onscreen
                   )
           )
          )

;;=======================================================================================================

